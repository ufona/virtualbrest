/**
 * VirtualBrest List Page
 * https://github.com/ufon
 * @ufona Anton Postoyalko
 */

import React, { Component } from 'react';

import {
  AppRegistry,
  StyleSheet,
  Text,
  Alert,
  TouchableNativeFeedback,
  TouchableHighlight,
  BackAndroid,
  Dimensions
} from 'react-native';

import {
  Image,
  ListView,
  Tile,
  View,
  Title,
  Subtitle,
  Caption,
  Card,
  GridRow,
  Divider,
  TouchableOpacity,
  ScrollView
} from '@shoutem/ui';

import { Actions } from 'react-native-router-flux';

import Icon from 'react-native-vector-icons/MaterialIcons';

import { Toolbar, Avatar, Button, Drawer, COLOR, TYPO } from 'react-native-material-design';

import GiftedListView from 'react-native-gifted-listview';

import * as Animatable from 'react-native-animatable';

import ActionButton from 'react-native-action-button';

class List extends Component {

  constructor(props) {
    super(props);

    //UIManager.setLayoutAnimationEnabledExperimental && UIManager.setLayoutAnimationEnabledExperimental(true);

    this.state = { lastIndex: 1, page: 1, catid: 0, isActionButtonVisible: false, actionButtonTrigger: -100, newsCount: 5 };

    //console.log(this.props.catid);

    BackAndroid.addEventListener('hardwareBackPress', () => { BackAndroid.exitApp() });

    const window = Dimensions.get('window');

    //this._onScroll = this._onScroll.bind(this);

    var _listViewOffset = 0;

  }

  // _onScroll(event) {
  //   // Simple fade-in / fade-out animation

  //   // Check if the user is scrolling up or down by confronting the new scroll position with your own one
  //   const currentOffset = event.nativeEvent.contentOffset.y
  //   const direction = (currentOffset > 0 && currentOffset > this._listViewOffset)
  //     ? 'down'
  //     : 'up'
  //   // If the user is scrolling down (and the action-button is still visible) hide it
  //   var isActionButtonVisible = direction === 'up'
  //   if (isActionButtonVisible !== this.state.isActionButtonVisible) {

  //     LayoutAnimation.easeInEaseOut();
  //     this.setState({ isActionButtonVisible: isActionButtonVisible, actionButtonTrigger: direction == "up" ? 0 : -100 })

  //   }
  //   // Update your scroll position
  //   this._listViewOffset = currentOffset
  // }

  onFetch(page = 1, callback, options) {

    var responseData = [];
    var catid = this.props.catid;
    fetch('http://virtualbrest.by/rss/newsjson.php?idr=' + catid + '&ot=' + (1 + (page - 1) * 5) + '&kol=' + 5)
      .then((response) => response.json())
      .then((responseText) => {
        for (var i = 0; i < 5; i++) {
          responseData.push(responseText[i]);
        };
        page = page + 1;
        //console.log(responseData);
        callback(responseData);
      })
      .catch((error) => {
        Alert.alert(
          'Ошибка подключения',
          'Проверьте соединение с интернетом',
          [
            { text: 'OK', onPress: () => console.log('OK Pressed') },
          ]
        )
        console.warn(error);
      });
  }

  renderRowView(rowData) {

    //console.log(rowData);

    rowData.img = rowData.img != null ? rowData.img : 'http://ufon.brest.by/img/screen.png';

    return (


      <TouchableOpacity onPress={() => Actions.Post({ link: rowData.link, comments: rowData.comment })}>
        <Animatable.View animation="fadeIn" style={{ maxWidth: this.window.width, alignSelf: 'center' }} >
          <Tile >
            <Image
              styleName="large-banner"
              source={{ uri: rowData.img }}
              >
              <Tile style={{ backgroundColor: 'rgba(0,0,0,0.6)' }} >
                <Title style={{ fontFamily: 'RobotoCondensed' }} styleName="md-gutter-bottom">{rowData.title}</Title>
                <Caption style={{ fontFamily: 'RobotoCondensed' }} >{rowData.data}</Caption>
              </Tile>
            </Image>
            <View styleName="content">
              <View styleName="horizontal space-between">
                <Caption style={{ fontFamily: 'RobotoCondensed' }} >{rowData.comment}комментариев</Caption>

              </View>
            </View>
          </Tile>

          <Divider styleName="section-header" />
        </Animatable.View>
      </TouchableOpacity>

    );
  }

  openDrawer = () => {
    //this.refs.listview.scrollTo(0);
  }

  render() {

    //console.log(this.refs);

    return (

      <View style={{ flex: 1, marginTop: 55 }} >

        <GiftedListView

          //onScroll={this._onScroll.bind(this)}
          enableEmptySections={true}
          rowView={this.renderRowView}
          onFetch={this.onFetch.bind(this)}
          firstLoader={true}
          pagination={true}
          refreshable={true}
          autoPaginate={true}
          withSections={false} />




      </View>

    );

  }
}

const styles = StyleSheet.create({
  mainTitle: {
    fontFamily: 'ClearSans_regular',
    fontStyle: 'normal',
    fontWeight: '200',
    lineHeight: 27,
    paddingBottom: 3
  },
  overlay: {
    opacity: 0.5,
    backgroundColor: 'black',
  },
  header: {
    marginTop: 60
  },
  toolbar: {
    height: 56,
    backgroundColor: '#512DA8'
  },
  paginationView: {
    height: 44,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#2722a3',
  },
  refreshableView: {
    height: 50,
    backgroundColor: '#2722a3',
    justifyContent: 'center',
    alignItems: 'center',
  },
});


module.exports = List;