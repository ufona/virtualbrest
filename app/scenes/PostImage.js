/**
 * VirtualBrest Image in post
 * https://github.com/ufon
 * @ufona Anton Postoyalko
 */

import React, { Component } from 'react';

import {
  AppRegistry,
  StyleSheet,
  Text,
  DrawerLayoutAndroid,
  TouchableHighlight,
  ActivityIndicator,
  ScrollView,
  View,
  Image,
  Linking,
  Dimensions
} from 'react-native';

import * as Animatable from 'react-native-animatable';

import { Toolbar } from 'react-native-material-design';

import { Actions } from 'react-native-router-flux';

import PhotoView from 'react-native-photo-view';

const { width, height } = Dimensions.get('window')

class PostImage extends Component {

  constructor(props) {
    super(props);

    this.state = {
      link: this.props.link,
      loaded: false
    };
  }

  render() {

    const goBack = () => Actions.pop();

    return (

      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', backgroundColor: 'black' }}>


        <PhotoView
          source={{ uri: this.state.link }}
          resizeMode='contain'
          minimumZoomScale={1}
          maximumZoomScale={3}
          androidScaleType='fitCenter'
          style={styles.photo} />


      </View>

    );

  }

}

const styles = StyleSheet.create({
  article: {
    margin: 10,
    alignSelf: 'center'
  },
  photo: {
    flex: 1,
    width,
    height,


  }
});

module.exports = PostImage;