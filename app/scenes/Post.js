/**
 * VirtualBrest Post Page
 * https://github.com/ufon
 * @ufona Anton Postoyalko
 */

import React, { Component } from 'react';

import {
  AppRegistry,
  StyleSheet,
  Text,
  DrawerLayoutAndroid,
  TouchableHighlight,
  ActivityIndicator,
  ScrollView,
  View as ViewNative,
  Linking,
  BackAndroid,
  ToastAndroid,
  Clipboard
} from 'react-native';

import {
  Image,
  ListView,
  Tile,
  View,
  Title,
  Subtitle,
  Caption,
  Card,
  GridRow,
  Divider,
  TouchableOpacity,
  Icon,
  Button,
  Spinner
} from '@shoutem/ui';

import Modal from 'react-native-modalbox';

import * as Animatable from 'react-native-animatable';

import { Toolbar } from 'react-native-material-design';

import { Actions } from 'react-native-router-flux';

import HtmlRender from './htmlRender/htmlRender';

import store from 'react-native-simple-store';

class Post extends Component {

  constructor(props) {
    super(props);

    this.state = {
      link: this.props.link,
      responseData: '',
      loaded: false,
      fontSize: 14
    };

    BackAndroid.addEventListener('hardwareBackPress', () => {
      try {
        Actions.pop();
        return true;
      }
      catch (err) {
        BackAndroid.exitApp()
      }
    });


  }

  componentDidMount() {
    var link = this.props.link;

    store
      .get('fontSize')
      .then(fontSize => {
        this.setState({ fontSize: fontSize, link: link });

      });


    setTimeout(
      () => {
        fetch('http://virtualbrest.by/android.php?pdaid=' + link + '&json=1')
          .then((response) => response.json())
          .then((responseText) => {
            this.setState({ loaded: true, responseData: responseText });
          })
          .catch((error) => {
            console.warn(error);
          });
      }, 1000);
  }

  onLinkPress(url) {

    if (/^http?:\/\/virtual?(?:\.)?brest\.by?\/news.\d+.php/.test(url)) {

      var link = url.match(/\d+/)[0];

      Actions.Post({ link: link, comments: 'NaN' });

    } else {

      Linking.openURL(url)

    }

  }



  loaderView() {

    const goComments = () => Actions.Comments({ link: this.state.link });

    if (!this.state.loaded) {

      return (

        <ViewNative style={{
          flex: 1,
          justifyContent: 'center',
          alignItems: 'center',
        }}>

          <ActivityIndicator
            color="#2722a3"
            size="large"
            />

        </ViewNative>
      );

    } else {

      return (

        <ScrollView style={{ marginTop: 55 }}>
          <Animatable.View animation="fadeIn" >
            <Tile style={{ backgroundColor: 'rgba(0,0,0,0.2)', minHeight: 50 }} styleName="text-centric">
              <Title style={{ fontFamily: 'RobotoCondensed' }} styleName="sm-gutter-bottom">{this.state.responseData.title}</Title>
              <Caption style={{ fontFamily: 'RobotoCondensed' }} >{this.state.responseData.data}</Caption>
            </Tile>

            <Divider styleName="section-header" />
          </Animatable.View>
          <Animatable.View animation="fadeIn" style={{ margin: 15 }}>
            <HtmlRender
              value={this.state.responseData.content}
              onLinkPress={this.onLinkPress.bind(this)}
              fontSize={this.state.fontSize}
              />

          </Animatable.View>
          <Divider styleName="section-header">
            <Button onPress={goComments} styleName="full-width">
              <Text>{this.props.comments}комментариев</Text>
            </Button>
          </Divider>
          <Divider styleName="section-header" />
        </ScrollView>

      );

    }

  }

  setClipboard() {
    Clipboard.setString('http://virtualbrest.by/news' + this.state.link + '.php');

    ToastAndroid.show(
      'Ссылка на новость скопирована в буфер обмена',
      ToastAndroid.SHORT
    );

    Linking.openURL('http://virtualbrest.by/news' + this.state.link + '.php');
  };


  render() {

    const goBack = () => Actions.pop();

    return (

      <View style={{ flex: 1 }}>

        <Toolbar
          title={'Новость'}
          primary={'#2722a3'}
          icon={'arrow-back'}
          actions={[{
            icon: 'open-in-new',
            onPress: this.setClipboard.bind(this)
          }]}
          onIconPress={goBack} />

        {this.loaderView()}

      </View>

    );
  }

}

const styles = StyleSheet.create({
  modal: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  article: {
    margin: 10,
    alignSelf: 'center'
  },
  headerFont: {
    fontFamily: 'RobotoCondensedRegular'
  }
});

module.exports = Post;