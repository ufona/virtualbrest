/**
 * VirtualBrest Comments Page
 * https://github.com/ufon
 * @ufona Anton Postoyalko
 */

import React, { Component } from 'react';

import {
  AppRegistry,
  StyleSheet,
  Text,
  DrawerLayoutAndroid,
  TouchableHighlight,
  ActivityIndicator,
  ScrollView,
  View as ViewNative,
  Linking,
  ToastAndroid
} from 'react-native';

import {
  Image,
  ListView,
  Tile,
  View,
  Title,
  Subtitle,
  Caption,
  Card,
  GridRow,
  Divider,
  TouchableOpacity,
  Button,
  Spinner
} from '@shoutem/ui';

import * as Animatable from 'react-native-animatable';

import { Toolbar } from 'react-native-material-design';

import { Actions } from 'react-native-router-flux';

import Icon from 'react-native-vector-icons/MaterialIcons';

import HtmlRender from './htmlRender/htmlRender';

import ActionButton from 'react-native-action-button';

import CommentRow from './CommentRow';

class Comments extends Component {

  constructor(props) {
    super(props);

    this.state = {
      link: this.props.link,
      responseData: '',
      loaded: false
    };

    //UIManager.setLayoutAnimationEnabledExperimental && UIManager.setLayoutAnimationEnabledExperimental(true);

    this.refreshComments = this.refreshComments.bind(this);
  }

  componentDidMount() {

    var link = this.props.link;
    setTimeout(
      () => {
        fetch('http://virtualbrest.by/android_comm.php?id=' + link)
          .then((response) => response.json())
          .then((responseText) => {
            this.setState({ loaded: true, responseData: responseText, isActionButtonVisible: false, actionButtonTrigger: 0 });
          })
          .catch((error) => {
            console.warn(error);
          });
      }, 1000);

  }

  // _onScroll(event) {
  //   // Simple fade-in / fade-out animation
  //   // Check if the user is scrolling up or down by confronting the new scroll position with your own one
  //   const currentOffset = event.nativeEvent.contentOffset.y
  //   const direction = (currentOffset > 0 && currentOffset > this._listViewOffset)
  //     ? 'down'
  //     : 'up'
  //   // If the user is scrolling down (and the action-button is still visible) hide it
  //   var isActionButtonVisible = direction === 'up'
  //   if (isActionButtonVisible !== this.state.isActionButtonVisible) {

  //     LayoutAnimation.easeInEaseOut();
  //     this.setState({ isActionButtonVisible: isActionButtonVisible, actionButtonTrigger: direction == "up" ? 0 : -100 })

  //   }
  //   // Update your scroll position
  //   this._listViewOffset = currentOffset
  // }

  onLinkPress(url) {

    if (/^http?:\/\/virtual?(?:\.)?brest\.by?\/news.\d+.php/.test(url)) {

      var link = url.match(/\d+/)[0];

      Actions.Post({ link: link });

    } else {

      Linking.openURL(url)

    }

  }

  loaderView() {
    if (!this.state.loaded) {
      return (
        <View style={{
          flex: 1,
          justifyContent: 'center',
          alignItems: 'center',
        }}>

          <ActivityIndicator
            color="#2722a3"
            size="large"
            />
        </View>
      );
    } else {

      return (
        <ViewNative style={{ flex: 1 }}>
          <ScrollView style={{ marginTop: 55 }}  >

            <CommentRow children={this.state.responseData} />

          </ScrollView>

        </ViewNative>
      );

    }
  }

  refreshComments() {
    this.setState({ loaded: false });

    setTimeout(
      () => {
        fetch('http://virtualbrest.by/android_comm.php?id=' + this.state.link)
          .then((response) => response.json())
          .then((responseText) => {
            this.setState({ loaded: true, responseData: responseText, isActionButtonVisible: false, actionButtonTrigger: 0 });
          })
          .catch((error) => {
            console.warn(error);
          });
      }, 1000);
  }

  setClipboard() {

    ToastAndroid.show(
      'Скоро.',
      ToastAndroid.SHORT
    )

  }

  render() {

    const goBack = () => Actions.pop();

    return (

      <View style={{ flex: 1 }}>

        <Toolbar
          title={'Комментарии'}
          primary={'#2722a3'}
          icon={'arrow-back'}
          actions={[{
            icon: 'refresh',
            onPress: this.refreshComments
          }]}
          onIconPress={goBack} />


        {this.loaderView()}


      </View>

    );

  }

}

const styles = StyleSheet.create({
  article: {
    margin: 10,
    alignSelf: 'center'
  }
});

module.exports = Comments;