import React, { Component, PropTypes } from 'react'

import {
  StyleSheet,
  Text,
  Linking
} from 'react-native';

import {
  Row,
  Subtitle,
  Caption,
  View,
  Divider
} from '@shoutem/ui';

import * as Animatable from 'react-native-animatable';

import HTMLView from './htmlRenderComment/HTMLView';

export default class CommentRow extends Component {

  constructor(props) {
    super(props);
  }

  parseBB(str) {

    str = str.replace(/<br\s*\/?>/mg, "\n");

    str = str.replace(/\[(\/?)b\]/g, "<$1b>");

    str = str.replace(/\[(\/?)i\]/g, "<$1i>");

    str = str.replace(/\[(\/?)s\]/g, "<$1s>");

    str = str.replace(/\[(\/?)u\]/g, "<$1u>");

    str = str.replace(/\[url=([^\s\]]+)\s*\](.*(?=\[\/url\]))\[\/url\]/g, '<a href="$1">$2</a>');

    str = str.replace(/\[img=([^\s\]]+)\s*\]/g, '<a href="$1">$1</a>');

    str = str.replace(/\[(\/?)quote\]/g, "<$1quote>");

    str = "<div>" + str + "</div>";

    return (<HTMLView value={str} onLinkPress={this.onLinkPress.bind(this)} />)

  }

  ratingParse(rating) {

    rating = parseInt(rating);

    if (rating > 0) {

      return (<Text style={{ fontFamily: 'RobotoCondensed', color: 'green' }} >+{rating}</Text>);

    } else if (rating < 0) {

      return (<Text style={{ fontFamily: 'RobotoCondensed', color: 'red' }} >{rating}</Text>);

    } else {

      return (<Text style={{ fontFamily: 'RobotoCondensed' }} >{rating}</Text>);

    }


  }

  onLinkPress(url) {

    if (/^http?:\/\/virtual?(?:\.)?brest\.by?\/news.\d+.php/.test(url)) {

      var link = url.match(/\d+/)[0];

      Actions.Post({ link: link });

    } else {

      Linking.openURL(url)

    }

  }

  render() {

    const children = this.props.children;

    return (
      <View>
        {children.map((comment, i) =>
          <Animatable.View animation="fadeIn">
            <Row style={{ marginLeft: comment.level * 15 }} key={i}>

              <View styleName="vertical stretch space-between">
                <Subtitle style={{ fontFamily: 'RobotoCondensed' }} >{comment.name}</Subtitle>

                <Caption style={{ fontFamily: 'RobotoCondensed' }} >{comment.data}</Caption>

                <Text style={{ marginBottom: 5 }} styleName="multiline">{this.parseBB(comment.comm)}</Text>
                <Caption style={{ alignSelf: 'flex-end', marginBottom: 10 }} >{this.ratingParse(comment.raitng)}</Caption>
                <Divider styleName="line" />
              </View>

            </Row>

            {comment.childs && <CommentRow children={comment.childs} />}


          </Animatable.View>
        )}

      </View>
    )

  }

}

CommentRow.propTypes = {
  children: PropTypes.array.isRequired
}

const styles = StyleSheet.create({
  bold: {
    fontWeight: 'bold',
    fontFamily: 'PTSerif',
    color: 'rgba(0,0,0,0.85)'
  },
  quote: {
    paddingLeft: 10,
    borderLeftColor: '#3498DB',
    borderLeftWidth: 3
  }
});
