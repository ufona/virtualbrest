/**
 * VirtualBrest
 * https://github.com/ufon
 * @ufona Anton Postoyalko
 */

import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  BackAndroid,
  DrawerLayoutAndroid,
  TouchableHighlight
} from 'react-native';

import { Router, Scene } from 'react-native-router-flux';

var List = require('./app/scenes/List.js');

var Post = require('./app/scenes/Post.js');

var PostImage = require('./app/scenes/PostImage.js');

var Comments = require('./app/scenes/Comments.js');

var DrawerMain = require('./app/scenes/interface/Drawer.js');

var Settings = require('./app/scenes/Settings.js');

var FontSize = require('./app/scenes/FontSize.js');

class VirtualBrest extends Component {

  render() {



    return (
      <Router>
        <Scene key="root" hideNavBar={true} >
          <Scene key="drawer" component={DrawerMain} initial={true} >
            <Scene key="List" component={List} catid={0} />
          </Scene>

          <Scene key="Settings" component={Settings} />

          <Scene key="FontSize" direction="vertical" component={FontSize} />

          <Scene key="Post" component={Post} />
          <Scene key="Comments" direction="vertical" component={Comments} />
          <Scene key="PostImage" animation="fade" component={PostImage} />

        </Scene>
      </Router>
    )
  }
}

AppRegistry.registerComponent('VirtualBrest', () => VirtualBrest);
