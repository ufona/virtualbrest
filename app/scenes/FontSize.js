/**
 * VirtualBrest FontSize Page
 * https://github.com/ufon
 * @ufona Anton Postoyalko
 */

import React, { Component } from 'react';

import {
  AppRegistry,
  StyleSheet,
  Text,
  DrawerLayoutAndroid,
  TouchableHighlight,
  ActivityIndicator,
  ScrollView,
  View as ViewNative,
  Linking,
  ToastAndroid,
  Slider,
  AsyncStorage
} from 'react-native';

import {
  View,
  Divider,
  TouchableOpacity,
  Row
} from '@shoutem/ui';

import * as Animatable from 'react-native-animatable';

import { Toolbar } from 'react-native-material-design';

import { Actions } from 'react-native-router-flux';

import Icon from 'react-native-vector-icons/MaterialIcons';

import DialogAndroid from 'react-native-dialogs';

import store from 'react-native-simple-store';

class FontSize extends Component {

  constructor(props) {
    super(props);

    this.state = {
      value: 14
    };

    //this._loadInitialState = this._loadInitialState.bind(this);

  }

  componentDidMount() {
    store.get('fontSize').then(fontSize => {
      this.setState({ value: fontSize });
    });
  }

  setFontSize(value) {

    this.setState({ value: value });

    store.save('fontSize', value).then(() => {

      ToastAndroid.show(
        'Сохранено.',
        ToastAndroid.SHORT
      )

    });

  }

  render() {

    const goBack = () => Actions.pop({ refresh: { fontSize: this.state.value } });

    const defaultVal = this.state.value !== null ? this.state.value : 14;

    var styles = StyleSheet.create({
      p: {
        lineHeight: this.state.value,
        fontSize: this.state.value,
        //marginTop: 15,
        //marginBottom: 15,
        fontFamily: 'PTSerif',
        paddingTop: 5,
        paddingBottom: 5,
        color: 'rgba(0,0,0,0.8)'
      }
    });

    return (

      <View style={{ flex: 1 }}>

        <Toolbar
          title={'Размер шрифта'}
          primary={'#2722a3'}
          icon={'arrow-back'}
          actions={[{
            icon: 'save',
            onPress: () => this.setFontSize(this.state.value)
          }]}

          onIconPress={goBack} />
        <ViewNative style={{ flex: 1 }}>
          <ScrollView style={{ marginTop: 55 }} >

            <ScrollView style={{ margin: 10, height: 300 }} >
              <Text style={styles.p}>Таким образом укрепление и развитие структуры в значительной степени обуславливает создание новых предложений. Равным образом консультация с широким активом позволяет оценить значение модели развития. Значимость этих проблем настолько очевидна, что постоянное информационно-пропагандистское обеспечение нашей деятельности влечет за собой процесс внедрения и модернизации направлений прогрессивного развития.</Text>
            </ScrollView>
            <Slider
              minimumValue={6}
              maximumValue={32}
              step={1}
              value={defaultVal}
              onValueChange={(value) => this.setState({ value: value })} />

            <Text style={{ textAlign: 'center', marginTop: 10 }}>{this.state.value}</Text>

          </ScrollView>




        </ViewNative>


      </View>

    );

  }

}

module.exports = FontSize;