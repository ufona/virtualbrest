/**
 * VirtualBrest Settings Page
 * https://github.com/ufon
 * @ufona Anton Postoyalko
 */

import React, { Component } from 'react';

import {
  AppRegistry,
  StyleSheet,
  Text,
  DrawerLayoutAndroid,
  TouchableHighlight,
  ActivityIndicator,
  ScrollView,
  BackAndroid,
  View as ViewNative,
  Linking,
  ToastAndroid,
  Slider,
  AsyncStorage
} from 'react-native';

import {
  View,
  Divider,
  TouchableOpacity,
  Row,
  Caption
} from '@shoutem/ui';

import * as Animatable from 'react-native-animatable';

import { Toolbar } from 'react-native-material-design';

import { Actions } from 'react-native-router-flux';

import Icon from 'react-native-vector-icons/MaterialIcons';

import DialogAndroid from 'react-native-dialogs';

import store from 'react-native-simple-store';

class Settings extends Component {

  constructor(props) {
    super(props);

    this.state = {
      fontSize: 14,
      newsCount: 5
    };

            BackAndroid.addEventListener('hardwareBackPress', () => {
        try {
            Actions.pop();
            return true;
        }
        catch (err) {
            BackAndroid.exitApp()
        }
    });

    this.indexes = { 3: 0, 5: 1, 10: 2, 15: 3, 20: 4 };

    this.options = {
      items: [
        "3",
        "5",
        "10",
        "15",
        "20"
      ],
      selectedIndex: 1,
      title: "Количество новостей",
      itemsCallback: (id, text) => this.setCountNews(text),
      negativeText: "Отмена"
    };

  }

  componentWillReceiveProps(params) {

    this.setState({ fontSize: params.fontSize });

  }

  componentDidMount() {

    store.get('fontSize').then(fontSize => {
      fontSize = fontSize !== null ? fontSize : 14
      this.setState({ fontSize: fontSize });
    });

    store.get('newsCount').then(newsCount => {

      newsCount = newsCount !== null ? newsCount : 5

      this.setState({ newsCount: newsCount });

      this.options.selectedIndex = this.indexes[this.state.newsCount];

    });

  }

  setCountNews(newsCount) {

    store.save('newsCount', newsCount).then(() => {

      this.setState({ newsCount: newsCount });

      this.options.selectedIndex = this.indexes[this.state.newsCount];

      ToastAndroid.show(
        'Сохранено: ' + newsCount + ' новостей на страницу.',
        ToastAndroid.SHORT
      )

    });

  }

  render() {

    const goBack = () => Actions.pop();

    const goFontSize = () => Actions.FontSize();

    const showDialog = () => {
      var dialog = new DialogAndroid();
      dialog.set(this.options);
      dialog.show();
    }

    var styles = StyleSheet.create({
      p: {
        lineHeight: this.state.value,
        fontSize: this.state.value,
        //marginTop: 15,
        //marginBottom: 15,
        fontFamily: 'PTSerif',
        paddingTop: 5,
        paddingBottom: 5,
        color: 'rgba(0,0,0,0.8)'
      },
      descAligment: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
      },
      desc: {
        width: 35,
        height: 35,
        margin: 20
      },
      logo: {
        width: 90,
        height: 33,
      }
    });

    return (

      <View style={{ flex: 1 }}>

        <Toolbar
          title={'Настройки'}
          primary={'#2722a3'}
          icon={'arrow-back'}
          actions={[{
            icon: 'refresh',
            onPress: this.setClipboard
          }]}

          onIconPress={goBack} />
        <ViewNative style={{ flex: 1 }}>
          <ScrollView style={{ marginTop: 55 }} >

            <TouchableOpacity onPress={showDialog}>
              <Row styleName="small" >
                <Icon size={18} name="cloud-download" />
                <Text style={{ paddingLeft: 10, color: 'rgba(0,0,0,0.8)', fontSize: 14 }}>Количество новостей на страницу: <Text style={{ fontWeight: 'bold', alignSelf: 'stretch', textAlign: 'right', color: 'rgba(0,0,0,0.8)' }} > {this.state.newsCount} </Text></Text>
              </Row>
            </TouchableOpacity>
            <Divider styleName="section-header" />
            <TouchableOpacity onPress={goFontSize}>
              <Row styleName="small" >
                <Icon size={18} name="format-size" />
                <Text style={{ paddingLeft: 10, color: 'rgba(0,0,0,0.8)', fontSize: 14 }}>Основной размер шрифта: <Text style={{ fontWeight: 'bold', alignSelf: 'stretch', textAlign: 'right', color: 'rgba(0,0,0,0.8)' }} > {this.state.fontSize} </Text></Text>
              </Row>
            </TouchableOpacity>
            <Divider styleName="section-header" />

            <View style={{ flex: 1, alignSelf: 'center', marginRight: 50, marginLeft: 50 }}>

              <Text style={{ textAlign: 'center', marginTop: 10, color: 'rgba(0,0,0,0.8)' }}>

                Данное приложение создано с согласия владельца сайта "Виртуальный Брест" - Андрея Кухарчика

            </Text>



            </View>

            <ViewNative style={styles.descAligment}>
              <TouchableOpacity onPress={() => Linking.openURL('https://twitter.com/virtualbrest')}>
                <View>
                  <Animatable.Image style={styles.desc} animation="fadeIn" source={require('./../img/twi.png')} />
                </View>
              </TouchableOpacity>

              <TouchableOpacity onPress={() => Linking.openURL('http://virtualbrest.by/')}>
                <View>
                  <Animatable.Image style={styles.logo} animation="fadeIn" source={require('./../img/logo2.png')} />
                </View>
              </TouchableOpacity>

              <TouchableOpacity onPress={() => Linking.openURL('https://vk.com/virtual.brest')}>
                <View>
                  <Animatable.Image style={styles.desc} animation="fadeIn" source={require('./../img/vk.png')} />
                </View>
              </TouchableOpacity>


            </ViewNative>





          </ScrollView>

          <Divider styleName="section-header">
            <TouchableOpacity onPress={() => Linking.openURL('http://ufon.brest.by')}>
              <Caption style={{ margin: 10 }}>
                <Text style={{ fontSize: 10, fontFamily: 'RobotoCondensed', textAlign: 'center', marginTop: 20, color: 'rgba(0,0,0,0.8)' }} >
                  With ♥ by <Text style={{ textDecorationLine: 'underline', color: 'rgba(0,0,0,1)' }} > ufon.brest.by</Text>
                </Text>
              </Caption>
            </TouchableOpacity>
          </Divider>


        </ViewNative>


      </View>

    );

  }

}

module.exports = Settings;