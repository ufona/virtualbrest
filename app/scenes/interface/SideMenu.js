/**
 * VirtualBrest Main Page
 * https://github.com/ufon
 * @ufona Anton Postoyalko
 */

import React, { Component } from 'react';

import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    BackAndroid,
    Image,
    TouchableHighlight,
    TouchableNativeFeedback
} from 'react-native';

import { Actions } from 'react-native-router-flux';

import { Toolbar, Avatar, Card, Button, Drawer, Divider, COLOR, TYPO } from 'react-native-material-design';

var SideMenu = function () {

    var goToCategory = function (title, catid) {
        Actions.refresh({ key: 'drawer', title: title, catid: catid, doClose: true });
    };

    var goToSettings = function () {
        Actions.Settings()
    };

    return (
        <Drawer theme='light'>
            <Drawer.Header image={<Image source={require('./img/nav.png')} />}>
                <View style={styles.header}>
                    <Image style={styles.image} source={require('./img/logo.png')} />
                </View>
            </Drawer.Header>
            <Drawer.Section
                items={[{
                    icon: 'star',
                    value: 'Главные новости',
                    onPress: () => { goToCategory('Главные новости', 0) },
                }]} />
            <Drawer.Section
                title="Категории"
                items={[{
                    icon: 'navigate-next',
                    value: 'Брестская область',
                    onPress: () => { goToCategory('Брестская область', 51) },
                },
                {
                    icon: 'navigate-next',
                    value: 'Авто, ГАИ, Дороги',
                    onPress: () => { goToCategory('Авто, ГАИ, Дороги', 36) },
                },
                {
                    icon: 'navigate-next',
                    value: 'Граница, таможня',
                    onPress: () => { goToCategory('Граница, таможня', 38) },

                },
                {
                    icon: 'navigate-next',
                    value: 'История города Бреста',
                    onPress: () => { goToCategory('История города Бреста', 47) },

                },
                {
                    icon: 'navigate-next',
                    value: 'Благотворительность',
                    onPress: () => { goToCategory('Благотворительность', 56) },

                },
                {
                    icon: 'navigate-next',
                    value: 'Криминал происшествия',
                    onPress: () => { goToCategory('Криминал происшествия', 44) },

                },
                {
                    icon: 'navigate-next',
                    value: 'Разное',
                    onPress: () => { goToCategory('Разное', 86) },

                },
                {
                    icon: 'navigate-next',
                    value: 'Праздники, события',
                    onPress: () => { goToCategory('Праздники, события', 41) },
                },
                {
                    icon: 'navigate-next',
                    value: 'Виртуальная экскурсия',
                    onPress: () => { goToCategory('Виртуальная экскурсия', 77) },
                },
                {
                    icon: 'navigate-next',
                    value: 'Брест - культурная столица',
                    onPress: () => { goToCategory('Брест - культурная столица', 27) },
                },
                {
                    icon: 'navigate-next',
                    value: 'Музыка, искусство, культура',
                    onPress: () => { goToCategory('Музыка, искусство, культура', 39) },
                },
                {
                    icon: 'navigate-next',
                    value: 'Брестская крепость',
                    onPress: () => { goToCategory('Брестская крепость', 29) },
                },
                {
                    icon: 'navigate-next',
                    value: 'Беловежская пуща',
                    onPress: () => { goToCategory('Беловежская пуща', 30) },
                },
                {
                    icon: 'navigate-next',
                    value: 'Гостиницы, отдых, туризм',
                    onPress: () => { goToCategory('Гостиницы, отдых, туризм', 37) },
                },
                {
                    icon: 'navigate-next',
                    value: 'Медицина, здоровье',
                    onPress: () => { goToCategory('Медицина, здоровье', 45) },
                },
                {
                    icon: 'navigate-next',
                    value: 'Здоровая нация',
                    onPress: () => { goToCategory('Здоровая нация', 50) },
                },
                {
                    icon: 'navigate-next',
                    value: 'Фото обзоры о Бресте',
                    onPress: () => { goToCategory('Фото обзоры о Бресте', 32) },
                },
                {
                    icon: 'navigate-next',
                    value: 'Политика, власть',
                    onPress: () => { goToCategory('Политика, власть', 42) },
                },
                {
                    icon: 'navigate-next',
                    value: 'Стихия, природа, животные',
                    onPress: () => { goToCategory('Стихия, природа, животные', 78) },
                },
                {
                    icon: 'navigate-next',
                    value: 'Велосипедный Брест',
                    onPress: () => { goToCategory('Велосипедный Брест', 79) },
                },
                {
                    icon: 'navigate-next',
                    value: 'Недвижимость',
                    onPress: () => { goToCategory('Недвижимость', 1) },
                },
                {
                    icon: 'navigate-next',
                    value: 'Бизнес, экономика',
                    onPress: () => { goToCategory('Бизнес, экономика', 34) },
                },
                {
                    icon: 'navigate-next',
                    value: 'Интернет',
                    onPress: () => { goToCategory('Интернет', 40) },
                },
                {
                    icon: 'navigate-next',
                    value: 'Тайный покупатель',
                    onPress: () => { goToCategory('Тайный покупатель', 71) },
                },
                {
                    icon: 'navigate-next',
                    value: 'Рекламные новости',
                    onPress: () => { goToCategory('Рекламные новости', 140) },
                }]}

                />
            <Drawer.Section
                title="Информация"
                items={[{
                    icon: 'settings',
                    value: 'Настройки',
                    onPress: () => { goToSettings() },
                },
                {
                    icon: 'exit-to-app',
                    value: 'Выход',
                    onPress: () => { BackAndroid.exitApp() },
                }]}
                />

        </Drawer>

    );

};

const styles = StyleSheet.create({
    header: {
        marginTop: 60
    },
    image: {
        flex: 1,
        width: 120,
        height: 44,
        resizeMode: 'stretch'

    }
});


module.exports = SideMenu;