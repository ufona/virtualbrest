
import React, { Component } from 'react';

import {
  AppRegistry,
  StyleSheet,
  Text,
  Alert,
  TouchableNativeFeedback,
  TouchableHighlight,
  ListView
} from 'react-native';

import {
  View,
  Image,
  Card,
  Caption,
  Subtitle,
} from '@shoutem/ui';

import { Actions } from 'react-native-router-flux';

import { Toolbar, Avatar, Drawer, Divider, COLOR, TYPO } from 'react-native-material-design';

const Imager = props.img ? {uri: props.img} : require('./../img/welcome.jpg');

const Row = (props) => (
        <TouchableNativeFeedback onPress={() => Actions.Post({title: 'Hello World!'})}>
        <Animatable.View  animation="fadeIn">
        <Card >
        <Card.Media 
          height={220}
          image={<Animatable.Image animation="fadeIn" source={Imager} />}
          overlay>
                      <Text>{props.title}</Text>  
                      </Card.Media>
                      <Card.Body>
                      <View style={styles.descAligment}>

                           <Text style={[TYPO.paperSubhead, COLOR.paperGrey600, styles.descTitle]}>{props.data}</Text>
                          
                      </View>
                      </Card.Body>
        </Card>

        </Animatable.View>

      </TouchableNativeFeedback>
);

export default Row;