/**
 * VirtualBrest NavigationDrawer
 * https://github.com/ufon
 * @ufona Anton Postoyalko
 */

import React, { Component } from 'react';

import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  TouchableHighlight,
  DrawerLayoutAndroid
} from 'react-native';

import { Toolbar } from 'react-native-material-design';

import { Actions, DefaultRenderer } from 'react-native-router-flux';

import SideMenu from './SideMenu';


class DrawerMain extends Component {

  render() {

    const state = this.props.navigationState;

    const children = state.children;

    const catid = this.props.navigationState.catid ? this.props.navigationState.catid : 0;

    Actions.List({ catid: catid });

    state.title = state.title != null ? state.title : 'Главные новости';

    //console.log(children);

    //console.log(this.props.navigationState);

    if (state.doClose)
      this.drawer.closeDrawer();

    return (

      <DrawerLayoutAndroid
        ref={(drawer) => { return this.drawer = drawer } }
        drawerWidth={300}
        drawerPosition={DrawerLayoutAndroid.positions.Left}
        renderNavigationView={SideMenu} >

        <Toolbar
          title={state.title}
          primary={'#2722a3'}
          icon={'menu'}
          onIconPress={() => this.drawer.openDrawer()} />

        <DefaultRenderer navigationState={children[children.length - 1]} onNavigate={this.props.onNavigate} />

      </DrawerLayoutAndroid>

    );
  }
}

module.exports = DrawerMain;